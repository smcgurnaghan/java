package com.veracode.demo;

import org.springframework.web.util.HtmlUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;

@Controller
public class DemoController {

    @RequestMapping("/greeting")
    public String greeting(@RequestParam(value = "name", required = false, defaultValue = "User") String name, Model model) {
        model.addAttribute("greetingName", HtmlUtils.htmlEscape(name));
        return "greeting";
    }

    @RequestMapping("/howareyou")
    public String howAreYou(@RequestParam(value = "name", required = false, defaultValue = "User") String name, Model model) {
        model.addAttribute("howAreYouName", HtmlUtils.htmlEscape(name));
        return "howareyou";
    }

    @RequestMapping("/farewell")
    public String farewell(@RequestParam(value = "name", required = false, defaultValue = "User") String name, Model model) {
        model.addAttribute("farewellName", HtmlUtils.htmlEscape(name));
        return "farewell";
    }

}
